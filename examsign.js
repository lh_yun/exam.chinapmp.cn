﻿SHOW.extend(SHOW.User, {
    Examsign: {
        Init: function() {
            if ($("#vform").length > 0) { valt = new Validation("vform", { immediate: true, useTitles: true, stopOnFirst: true }); }
            SHOW.User.Examsign.InitData();
            if (typeof UINFO != "undefined") {
                try {
                    $("#Birthday").datepicker({ dateFormat: "yy-mm-dd", showMonthAfterYear: true, changeMonth: true, changeYear: true, yearRange: "c-80:c", minDate: "-80y", maxDate: "-10y" }, $.datepicker.regional['zh-CN']);
                    $("#Graduation").datepicker({ dateFormat: "yy-mm-dd", showMonthAfterYear: true, changeMonth: true, changeYear: true, yearRange: "c-60:c+5", minDate: "-60y", maxDate: "+5y" }, $.datepicker.regional['zh-CN']);
                } catch (e) { alert(e); }
            }
            $("input.btn_info").click(SHOW.User.Examsign.Saveinfo);
            $("#IsPMIUser input").click(SHOW.User.Examsign.IsUser);
            $("#Stype input").click(SHOW.User.Examsign.Ptitle);
            try {
                $("#PMIUtimeB,#PMIUtimeE,#PMItimeB,#PMItimeE").datepicker({ dateFormat: "yy-mm-dd", showMonthAfterYear: true, changeMonth: true, changeYear: true, yearRange: "c-3:c+3", minDate: "-3y", maxDate: "3y" }, $.datepicker.regional['zh-CN']);
            } catch (e) { }
            $("input.btn_sign").click(SHOW.User.Examsign.Sign);

            $('#clause_yes').click(function () {
                SHOW.Ajax.User.Examsign.Agree(EXAM.Id, function () { });
                $('#clause').hide();
                $('#vform').show();
            });
            $('#clause_no').click(function () { location.href = '//user.chinapmp.cn/index.shtml'; });
            SHOW.User.Examsign.Timer(60);
        },
        Timer: function (time) {
            if (time == 0) {
                $('#clause_yes').html(['我已阅读并同意签署'].join(''));
                $('#clause_yes').removeAttr('disabled');
                $('#clause_no').removeAttr('disabled');
            }
            else {
                $('#clause_yes').html(['我已阅读并同意签署（', time, '）'].join(''));
                setTimeout(function () { SHOW.User.Examsign.Timer(time - 1); }, 1000);
            }
        },
        InitData: function() { if (typeof UINFO != "undefined") { COM.SetData(UINFO); } },
        Saveinfo: function() {
            if (!valt.validate()) { return; }
            var Tname = $F("#Tname");
            var Gender = $FRV("#Gender"); if (Gender == null) { jWrong("请选择性别", "报名提示:"); return; }
            var Birthday = $FDT("#Birthday"); if (Birthday == null) { jWrong("请选择出生日期", "报名提示:"); return; }
            var IDtype = $FRV("#IDtype"); if (IDtype == null) { jWrong("请选择证件类型", "报名提示:"); return; }
            var IDnumber = $F("#IDnumber");
            var Graduated = $F("#Graduated");
            var Graduation = $FDT("#Graduation"); if (Graduation == null) { jWrong("请填毕业时间", "报名提示:"); return; }
            var Major = $F("#Major");
            var Edu = $FRV("#Edu"); if (Edu == null) { jWrong("请选择最高学历", "报名提示:"); return; }
            var Industry = $F("#Industry");
            var Workunits = $F("#Workunits");
            var Workunitstype = $F("#Workunitstype");
            var Position = $F("#Position");
            var Tel = $F("#Tel");
            var Mobile = $F("#Mobile");
            var Email = $F("#Email");
            var Amail = $F("#Amail");
            var Address = $F("#Address");
            var Zip = $F("#Zip");
            var btn = $(this); btn.attr("disabled", true);
            SHOW.Ajax.User.Examsign.SaveInfo(Tname, Gender, Birthday, IDtype, IDnumber, Graduated, Graduation, Major, Edu, Industry, Workunits, Workunitstype, Position, Tel, Mobile, Email, Amail, Address, Zip, function(Call) {
                btn.attr("disabled", false); if (!jError(Call)) { return; }
                if (Call.value) { window.location.href = SHOW.Config.PName + ";sign" + SHOW.Config.Ext; }
            });
        },
        IsUser: function() { if ($FRBOOL("#IsPMIUser")) { $(".ForIsPMIUser").show(); } else { $(".ForIsPMIUser").hide(); } },
        Ptitle: function() {
            $("tr.ForStype").show();
            var v = $(this).val();
            if (parseInt(v) == 101) {
                $("span.Ptitle").html("35");
                if (!$("#Peixunjigou").hasClass("validate-selection")) { $("#Peixunjigou").addClass("validate-selection"); }
            } else if (parseInt(v) == 102) {
                $("span.Ptitle").html("23");
                if ($("#Peixunjigou").hasClass("validate-selection")) { $("#Peixunjigou").removeClass("validate-selection"); }
            } else if (parseInt(v) == 103) {
                $("tr.ForStype").hide();
                $("span.Ptitle").html("35"); if ($("#Peixunjigou").hasClass("validate-selection")) { $("#Peixunjigou").removeClass("validate-selection"); }
            } else if (parseInt(v) == 104 || parseInt(v) == 105) {
                if (UINFO["Edu"] == "专科及以下") { $("span.Ptitle").html("40"); } else { $("span.Ptitle").html("30"); }
                if (!$("#Peixunjigou").hasClass("validate-selection")) { $("#Peixunjigou").addClass("validate-selection"); }
            } else {
                $("span.Ptitle").html("35");
                if (!$("#Peixunjigou").hasClass("validate-selection")) { $("#Peixunjigou").addClass("validate-selection"); }
            }
        },
        Sign: function() {
            if (!valt.validate()) { return; }
            var Ed = parseInt($F("#Ed"));
            var Etitle = $F("#Etitle");
            var Stype = parseInt($FRV("#Stype")) || 0; if ($FRV("#Stype") == null || Stype == 0) { alert("请选择考试类型"); return; }
            var StypeName = $TRV("#Stype");
            var Xing = $F("#Xing");
            var Zhong = $F("#Zhong");
            var Ming = $F("#Ming");
            var Peixunjigou = Stype != 103 ? $F("#Peixunjigou") : 0;
            var Peixunjigouming = Stype != 103 ? $TS("#Peixunjigou") : "";
            var PMIUname = $F("#PMIUname");
            var PMIUpass = $F("#PMIUpass");
            var IsPMIUser = $FRBOOL("#IsPMIUser");
            var PMINumber = IsPMIUser ? $F("#PMINumber") : ""; if (IsPMIUser && (IsPMIUser == null || IsPMIUser == "")) { alert("请填写PMI会员号"); return; }
            var PMIUtimeB = IsPMIUser ? $F("#PMIUtimeB") : ""; if (IsPMIUser && (PMIUtimeB == null || PMIUtimeB == "")) { alert("请填写PMI会员有效期"); return; }
            var PMIUtimeE = IsPMIUser ? $F("#PMIUtimeE") : ""; if (IsPMIUser && (PMIUtimeE == null || PMIUtimeE == "")) { alert("请填写PMI英文有效期"); return; }
            if (IsPMIUser) { if ($FDT("#PMIUtimeE") < EXAM["YuyueE"] || $FDT("#PMIUtimeB") > EXAM["YuyueE"]) { if (!confirm("您的会员有效时期并未包含预约截止时间，仍需按非会员的费用收取！\r\n\r\n是否继续？")) { return; } } }
            var PMItimeB = $FDT("#PMItimeB"); if (PMItimeB == null || PMItimeB == "") { alert("请填写PMI英文有效期"); return; }
            var PMItimeE = $FDT("#PMItimeE"); if (PMItimeE == null || PMItimeE == "") { alert("请填写PMI英文有效期"); return; }
            if (PMItimeE < EXAM["Edate"] || PMItimeB > EXAM["Edate"]) { alert("您的PMI英文有效期没有包括考试日期，不能报考！"); return; }
            var Kaodian = $F("#Kaodian");
            var Kaodianming = $TS("#Kaodian");
            var PMIID = $F("#PMIID");

            var btn = $(this); btn.attr("disabled", true);
            SHOW.Ajax.User.Examsign.Sign(Ed, Etitle, Stype, StypeName, Xing, Zhong, Ming, Peixunjigou, Peixunjigouming, PMIUname, PMIUpass, IsPMIUser, PMINumber, PMIUtimeB, PMIUtimeE, PMItimeB, PMItimeE, Kaodian, Kaodianming, PMIID, function(Call) {
                btn.attr("disabled", false); if (!jError(Call)) { return; }
                if (Call.value != null) {
                    jAlert("您已报名成功，此次考试您应缴纳的费用为" + Call.value + "，您的材料会在3天之内审核，请耐心等待！", "报名提示:", function() { window.location.href = "myexam" + SHOW.Config.Ext; });
                } else {
                    jWrong("报名失败！", "报名提示:");
                }
            });
        }
    }
});
$().ready(function() { SHOW.User.Examsign.Init(); });